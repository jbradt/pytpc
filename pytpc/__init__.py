"""A package for simulating, reading, and analyzing TPC data."""

from .constants import *
import pytpc.tracking
import pytpc.simulation
import pytpc.evtdata
import pytpc.tpcplot