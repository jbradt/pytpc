.. PyTPC documentation master file, created by
   sphinx-quickstart on Wed Jan 28 19:59:11 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyTPC's documentation!
=================================

Contents:

.. toctree::
   :maxdepth: 2

   evtdata
   relativity



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

